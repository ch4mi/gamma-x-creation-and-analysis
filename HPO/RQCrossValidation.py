import pandas as pd
import numpy as np

randomState = 42

import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score, auc, accuracy_score, confusion_matrix

# Assumes you have preprocessed and saved data, and have initialized and saved a XGB object beforehand.
# Load the data and model

dTrain = xgb.DMatrix('dTrain.buffer')
dTest = xgb.DMatrix('dTest.buffer')
BST = xgb.Booster()
BST.load_model('RQModel.model')

# Initial parameters

params = {'max_depth'         : 6,
          'min_child_weight'  : 3,
          'eta'               : 0.08,
          'objective'         : 'binary:logistic',
          'subsample'         : 0.9,
          'colsample_bylevel' : 1,
          'min_split_loss'    : 0.5, # "Gamma" in the XGBoost paper
          'lambda'            : 1, # L2 regularization
         } 

progress = dict()
watchlist =  [(dTrain, 'train'), (dTest, 'validation')]

nBoosts = 1000 # Maximum number of rounds

### GRID SEARCH ###
gridSearchParams = [.3, .2, .1, .05, .01, .005]

minError = float('inf')
bestParams = None

for eta in gridSearchParams:
    print('\n#########################################################################')
    print("Cross validation with eta = {}".format(eta))
    params['eta'] = eta
    
    trainingResults = xgb.train(params, dTrain, nBoosts,
                                evals = watchlist,
                                verbose_eval = False,
                                early_stopping_rounds = 50,
                                evals_result = progress)
    
    meanError = float(trainingResults.attributes().get('best_score'))
    bestBoostedRounds = int(trainingResults.attributes().get('best_iteration'))
    
    print('\tMean error {} for {} rounds'.format(meanError, bestBoostedRounds))
    
    # Update best parameters
    if meanError < minError:
        minError = meanError
        bestParams = eta
        
    print("Best parameters: {}, Error: {}".format(bestParams, minError))