import sys
import uproot
import awkward as awk
from waveformFuncs import getS1PodIndices, getS1PulsePodIndices, getS1Waveforms

inputFilePath = "/global/cfs/cdirs/lz/users/grischbi/MSSI/rfr_ffr_sims/"

if sys.argv[1] == 'FFR':
    fileList = ['FFR_output_20200410.root',
                'FFR_output_20200411.root',
                'FFR_output_20200412.root',
                'FFR_output_20200413.root',
                'FFR_output_20200414.root',
                'FFR_output_20200416.root',
                'FFR_output_20200418.root',
                'FFR_output_20200419.root']
elif sys.argv[1] == 'RFR':
    fileList = ['RFR_output_20200410.root',
                'RFR_output_20200411.root',
                'RFR_output_20200412.root',
                'RFR_output_20200413.root',
                'RFR_output_20200414.root',
                'RFR_output_20200416.root',
                'RFR_output_20200418.root']
else:
    raise Exception("Command line input must be 'RFR' or 'FFR'.")
    
    
for i, array in enumerate(uproot.iterate([inputFilePath + file for file in fileList],
                                         treepath = 'PodWaveforms',
                                         branches = 'summedPodsTPCHG.podSamples',
                                         outputtype = tuple,
                                         entrysteps = float("inf"))):

    
    print('\n Converting file %s waveforms into jagged array' % str(i+1))
    jaggedSamplesPod = awk.fromiter(array[0])
    print('Done!\n')
    
    awk.save('/global/cfs/cdirs/lz/users/chami/GammaX/RFRvsFFRWaveforms/waveformData/%s_File_%s' % (sys.argv[1], str(i+1)), jaggedSamplesPod)