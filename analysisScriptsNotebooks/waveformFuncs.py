import numpy as np
import awkward as awk

# -------------------------------------------------------------------------------------------------------------------------

def getS1PodIndices(pulseStartTimes_, podStartTimes_, podSamples_):
    """Returns POD indices for just the S1 pulse
        Arguments: 
            1D array of S1 pulse start times: EventVector< S1PulseStartTime >
            2D array of Pod start times:      EventVector< PodVector<PodStartTime> >
            3D array of Pod waveforms:        EventVector< PodVector< AmplitudeVector< PulseHeight >> >
        Returns:
            1D array of S1 Pod indices: EventVector< S1PodIndex >
    """
    
    podIndices = []
    for pulseStartTime, podStartTimesInEvent, waveformsInEvent in zip(pulseStartTimes_, podStartTimes_, podSamples_):
        # 1D array of Pod end times in the event
        podEndTimes = podStartTimesInEvent + waveformsInEvent.counts * 10
        podIndex = np.where( np.logical_and(pulseStartTime > podStartTimesInEvent, pulseStartTime < podEndTimes) )[0][0]
        
        podIndices.append(podIndex)
        
    return np.array(podIndices, dtype = int)

# -------------------------------------------------------------------------------------------------------------------------

def getS1PulsePodIndices(pulseStartTimes_, pulseEndTimes_, podIndices_, podStartTimes_):
    """Calculates pulse start and stop indices for just the S1 pulse
        Arguments: 
            1D array of S1 pulse start times: EventVector< S1PulseStartTime >
            1D array of S1 pulse end times:   EventVector< S1PulseEndTime >
            1D array of S1 POD:               EventVector< S1PodIndex >
            2D array of Pod start times:      EventVector< PodVector<PodStartTime> >
        Returns:
            2D array of S1 pulse start and stop indices: EventVector< Vector2D< PulseStartIndex, PulseStopIndex > >
    """
    s1PodStartTimes_ = podStartTimes_[[*range(len(podIndices_))], podIndices_]
    
    pulseStartIndex = ((pulseStartTimes_ - s1PodStartTimes_) / 10).astype(int)
    pulseEndIndex = ((pulseEndTimes_ - s1PodStartTimes_) / 10).astype(int)
    
    return np.dstack([pulseStartIndex, pulseEndIndex])[0]

# -------------------------------------------------------------------------------------------------------------------------

def getS1Waveforms(podSamples_, podIndices_, pulsePodIndices_):
    """Calculates S1 waveforms
        Arguments: 
            3D array of Pod waveforms:                   EventVector< PodVector< AmplitudeVector< PulseHeight >> >
            1D array of S1 POD:                          EventVector< S1PodIndex >
            2D array of S1 pulse start and stop indices: EventVector< Vector2D< PulseStartIndex, PulseStopIndex > >
        Returns:
            2D array of S1 pulse waveforms: EventVector< AmplitudeVector< PulseHeight > >
    """
    # Array of S1 waveform arrays
    s1PodSamples = podSamples_[[*range(len(podIndices_))], podIndices_]
    
    s1Waveforms = []
    for s1Pod, (pulsePodIndexMin, pulsePodIndexMax) in zip(s1PodSamples, pulsePodIndices_):
        s1Waveforms.append(s1Pod[pulsePodIndexMin : pulsePodIndexMax])
        
    # Second python bottleneck (not nearly as bad as the first)
    return np.array(s1Waveforms)