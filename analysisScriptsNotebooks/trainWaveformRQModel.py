from datetime import datetime

import numpy as np
import pandas as pd
import pickle

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score, auc, accuracy_score, confusion_matrix

randomState = 42

from tensorflow.keras.preprocessing import sequence
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Input, Dense, Activation, Dropout, Conv1D, MaxPooling1D, Flatten, Masking
from tensorflow.keras.metrics import AUC, Accuracy
from tensorflow.keras.layers import concatenate

def readData():
    """Read in RQ and waveform data from pickles.
       Returns the gX and FFR RQs and waveforms."""
    
    ### Read in RQ data ###
    dfFFR = pd.read_pickle('../intermediatePickles/FFRRQs.pkl')
    dfgX = pd.read_pickle('../intermediatePickles/gXRQs.pkl')
    # Extract relevant RQs
    gXRQs = dfgX[['TBA', 'clusterSize', 'topPeakFraction', 'bottomPeakFraction']].to_numpy(dtype = float)
    FFRRQs = dfFFR[['TBA', 'clusterSize', 'topPeakFraction', 'bottomPeakFraction']].to_numpy(dtype = float)
    print('%i gX RQs loaded' % len(dfgX))
    print('%i FFR RQs loaded\n' % len(dfFFR))
    
    ### Read in waveform data ###
    # Create file names
    waveformFilePath = '/global/cfs/cdirs/lz/users/chami/GammaX/RFRvsFFRWaveforms/waveformData/'
    waveformFileListFFR = ['FFR_1_waveforms.npy',
                           'FFR_2_waveforms.npy',
                           'FFR_3_waveforms.npy',
                           'FFR_4_waveforms.npy',
                           'FFR_5_waveforms.npy',
                           'FFR_6_waveforms.npy',
                           'FFR_7_waveforms.npy',
                           'FFR_8_waveforms.npy']
    waveformPathsFFR = [waveformFilePath + file for file in waveformFileListFFR]
    
    # Read in FFR waveforms
    waveformsFFR = np.load(waveformPathsFFR[0], allow_pickle = True)
    for file in waveformPathsFFR[1:]:
        waveformsFFR = np.concatenate((waveformsFFR, np.load(file, allow_pickle = True)))
    # Read in gX waveforms
    with open("../intermediatePickles/gXWaveforms.pkl", 'rb') as f:
        gXWaveforms = pickle.load(f)
    print('%i gX waveforms loaded' % len(gXWaveforms))
    print('%i FFR waveforms loaded\n' % len(waveformsFFR))
    
    if len(gXWaveforms) != len(dfgX):
        raise Exception("Gamma X RQs and waveforms don't match!")
    if len(waveformsFFR) != len(dfFFR):
        raise Exception("FFR RQs and waveforms don't match!")
        
    return (gXRQs, FFRRQs, gXWaveforms, waveformsFFR)
    
def preprocessData(data):
    """Takes in RQs and FFR for each population and returns Keras-ready input data."""
    
    gXRQs, FFRRQs, gXW, FFRW = data
    
    ### Normalize waveforms ###
    gXWNormalized = np.array([waveform / waveform.sum() for waveform in gXW])
    FFRWNormalized = np.array([waveform / waveform.sum() for waveform in FFRW])
    
    ### Balance the gX and FFR data ###
    nSamplesEach = min(len(gXWNormalized), len(FFRWNormalized))
    gXWNormalized = gXWNormalized[:nSamplesEach]
    FFRWNormalized = FFRWNormalized[:nSamplesEach]
    gXRQs = gXRQs[:nSamplesEach]
    FFRRQs = FFRRQs[:nSamplesEach]
    
    ### Concatenate gX and FFR into one dataset ###
    waveformData = np.concatenate((gXWNormalized, FFRWNormalized))
    RQData = np.concatenate((gXRQs, FFRRQs))
    
    ### Create truth values ###
    # gX is considered signal; assigned truth value 1, FFR assigned 0
    truth = np.concatenate((np.ones(nSamplesEach), np.zeros(nSamplesEach)))
    
    ### Waveform length handling ###
    # Truncate from start
    truncatedData = truncateFromStartOfWaveform(waveformData)
    # Truncate from end - if waveform is shorter than max length = 50, pad with zeros
    paddedData = sequence.pad_sequences(truncatedData, maxlen = 50, dtype = float, padding = 'post')
    
    ### Reshape for Keras ###
    reshapedPaddedData = np.reshape(paddedData, tuple([*np.shape(paddedData)] + [1]))
    reshapedRQData = np.reshape(RQData, tuple([*np.shape(RQData)]))
    
    ### Train test split ###
    waveformTrain, waveformTest, rqTrain, rqTest, yTrain, yTest = train_test_split(reshapedPaddedData, 
                                                                                   reshapedRQData, 
                                                                                   truth, 
                                                                                   test_size = 0.3,
                                                                                   random_state = randomState,
                                                                                   shuffle = True)
    return waveformTrain, waveformTest, rqTrain, rqTest, yTrain, yTest
    
def truncateFromStartOfWaveform(waveformArray):
    """Truncates the waveform from 100ns before AFT5."""
    truncatedWaveforms = []
    for i, waveform in enumerate(waveformArray):
        waveformCumSum = waveform.cumsum()
        # Get the index of wave amplitude 100ns before AFT5
        startIndex = max(0, np.argmax(waveformCumSum > 0.05) - 10)
        truncatedWaveforms.append(waveform[startIndex:])
    return np.array(truncatedWaveforms)

def createCNN(inputShape):
    """Returns Keras CNN ready to be trained."""
    ### First input - waveforms ###
    waveformInput = Input(shape = inputShape[1:], dtype = float)
    # Mask the zeros (from padding) from the network - this will be propagated down all layers
    maskingLayer = Masking(mask_value = 0.0)(waveformInput)

    conv1 = Conv1D(filters = 250, kernel_size = 5, activation = 'relu')(maskingLayer)
    maxPool1 = MaxPooling1D(pool_size = 2)(conv1)
    dropout1 = Dropout(0.3)(maxPool1)
    
   # conv2 = Conv1D(filters = 200, kernel_size = 7, activation = 'relu')(dropout1)
   # maxPool2 = MaxPooling1D(pool_size = 2)(conv2)
   # dropout2 = Dropout(0.3)(maxPool2)
    
    flatten = Flatten()(dropout1)
    
    ### Second input  - RQ information ###
    rqInput = Input(shape = (4,), dtype = float)
    
    # Concatenate inputs to feed into fully connected layers
    mergedVector = concatenate([flatten, rqInput])

    dense1 = Dense(40, activation = 'relu')(mergedVector)
    dense2 = Dense(10, activation = 'relu')(dense1)
    dense3 = Dense(1, activation = 'sigmoid')(dense2)
    
    # Create model
    model = Model(inputs = [waveformInput, rqInput], outputs = dense3)
    model.compile(loss = 'binary_crossentropy',
                  optimizer = 'Nadam',
                  metrics = ['accuracy', AUC(name = 'auc')])
    print(model.summary())
    
    return model

def main():
    
    print("Loading in data...")
    data = readData()
    print("Preprocessing data...")
    waveformTrain, waveformTest, rqTrain, rqTest, yTrain, yTest = preprocessData(data)
    
    print("Creating CNN...")
    waveformInputShape = (len(waveformTrain), len(waveformTrain[0]), 1)
    waveformCNN = createCNN(waveformInputShape)
    
    history = waveformCNN.fit(x = [waveformTrain, rqTrain],
                              y = yTrain,
                              validation_data = ([waveformTest, rqTest], yTest),
                              batch_size = 10,
                              epochs = 3)
    
    savedFileString = 'waveformModel_withRQs' + datetime.now().strftime("%d/%m_%H:%M:%S")
    waveformCNN.save('../models/' + savedFileString + '.h5')
    
    print('Model saved as', strftime("%m/%d/%Y, %H:%M:%S"))
    
    pickleFileString = '../intermediatePickles/data' + savedFileString + '.pkl'
    pickle.dump((waveformTrain, waveformTest, rqTrain, rqTest, yTrain, yTest), open(pickleFileString, 'wb'))
    
if __name__ == "__main__":
    main()