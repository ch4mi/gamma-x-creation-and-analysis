import sys
import numpy as np
import uproot
import awkward as awk
from waveformFuncs import getS1PodIndices, getS1PulsePodIndices, getS1Waveforms

waveformsFilePath = '/global/cfs/cdirs/lz/users/chami/GammaX/RFRvsFFRWaveforms/waveformData/'

if sys.argv[1] == 'FFR':
    waveformFileList = ['FFR_File_1.awkd',
                        'FFR_File_2.awkd',
                        'FFR_File_3.awkd',
                        'FFR_File_4.awkd',
                        'FFR_File_5.awkd',
                        'FFR_File_6.awkd',
                        'FFR_File_7.awkd',
                        'FFR_File_8.awkd']
    
elif sys.argv[1] == 'RFR':
    waveformFileList = ['RFR_File_1.awkd',
                        'RFR_File_2.awkd',
                        'RFR_File_3.awkd',
                        'RFR_File_4.awkd',
                        'RFR_File_5.awkd',
                        'RFR_File_6.awkd',
                        'RFR_File_7.awkd']
else:
    raise Exception("Command line input must be 'RFR' or 'FFR'.")
    
waveformFileList = [waveformsFilePath + file for file in waveformFileList]
    
rootFilePath = "/global/cfs/cdirs/lz/users/grischbi/MSSI/rfr_ffr_sims/"

if sys.argv[1] == 'FFR':
    fileList = ['FFR_output_20200410',
                'FFR_output_20200411',
                'FFR_output_20200412',
                'FFR_output_20200413',
                'FFR_output_20200414',
                'FFR_output_20200416',
                'FFR_output_20200418',
                'FFR_output_20200419']
elif sys.argv[1] == 'RFR':
    fileList = ['RFR_output_20200410',
                'RFR_output_20200411',
                'RFR_output_20200412',
                'RFR_output_20200413',
                'RFR_output_20200414',
                'RFR_output_20200416',
                'RFR_output_20200418']
else:
    raise Exception("Command line input must be 'RFR' or 'FFR'.")
    
rootFileList = [rootFilePath + file + '.root' for file in fileList]
MCTruthRootFileList = [rootFilePath + file + '_mctruth.root' for file in fileList]
    
pulseBranches = ['pulsesTPC.pulseStartTime_ns', 
                 'pulsesTPC.pulseEndTime_ns', 
                 'pulsesTPC.s1Probability',
                 'pulsesTPC.pulseArea_phd']    

waveformBranch = 'summedPodsTPCHG.podStartTime_ns'

    
for i, ((pulseStartTime, pulseEndTime, s1Probability, pulseArea), (podStartTime), (x)) in enumerate(
                                                                                     zip(
                                                                                     uproot.iterate(rootFileList,
                                                                                                     treepath = 'Events',
                                                                                                     branches = pulseBranches,
                                                                                                     outputtype = tuple,
                                                                                                     entrysteps = float("inf")), 
                                                                                     uproot.iterate(rootFileList,
                                                                                                     treepath = 'PodWaveforms',
                                                                                                     branches = waveformBranch,
                                                                                                     outputtype = tuple,
                                                                                                     entrysteps = float("inf")),
                                                                                     uproot.iterate(MCTruthRootFileList,
                                                                                                    treepath = 'RQMCTruth',
                                                                                                    branches = 'mcTruthVertices.positionX_mm',
                                                                                                    outputtype = tuple,
                                                                                                    entrysteps = float("inf"))
                                                                                     )):

    print("Starting waveform processing for file %s." % (i+1))

    jaggedWaveforms = awk.load(waveformFileList[i])

    ### Masks ###

    # S1 mask - cuts pulses in each event
    s1SelectionMask = s1Probability.argmax()
    if not all(i == 1 for i in s1Probability[s1SelectionMask].counts): raise Exception("More than one S1 in some events!")

    # Single scatter mask that is identically applied to event pairing - cuts entire events (using MCTruth)
    singleScatterMask = x[0].counts == 1

    pulseStartTime = pulseStartTime[s1SelectionMask][singleScatterMask].flatten()
    pulseEndTime = pulseEndTime[s1SelectionMask][singleScatterMask].flatten()
    pulseArea = pulseArea[s1SelectionMask][singleScatterMask].flatten()
    
    podStartTime = podStartTime[0]
    podStartTime = podStartTime[singleScatterMask]

    jaggedWaveforms = jaggedWaveforms[singleScatterMask]

    ### POD Selection ###

    s1PodIndices = getS1PodIndices(pulseStartTime, podStartTime, jaggedWaveforms)
    s1PulsePodIndices = getS1PulsePodIndices(pulseStartTime, pulseEndTime, s1PodIndices, podStartTime)
    s1Waveforms = getS1Waveforms(jaggedWaveforms, s1PodIndices, s1PulsePodIndices)
    
    with open(waveformsFilePath + sys.argv[1] + '_' + str(i+1) + '_waveforms.npy', 'wb') as f:
        np.save(f, s1Waveforms, allow_pickle = True)

    print("Done!\n")